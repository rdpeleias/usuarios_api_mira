FROM java:8
FROM maven:alpine

LABEL maintainer="rodrigo.peleias@gmail.com"
WORKDIR /app

COPY . /app

EXPOSE 8080
RUN mvn -v
ENTRYPOINT mvn spring-boot:run -Dspring-boot.run.profiles=container