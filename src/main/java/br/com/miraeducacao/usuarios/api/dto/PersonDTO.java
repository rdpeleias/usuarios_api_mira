package br.com.miraeducacao.usuarios.api.dto;

import br.com.miraeducacao.usuarios.api.validator.CheckDateFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
@Builder
@ApiModel
public class PersonDTO implements Serializable {

    private Long id;

    @NotEmpty(message = "{person.firstName.notempty}")
    @Size(min = 2, max = 100, message = "{person.firstName.size}")
    @ApiModelProperty(notes = "Nome", required = true, example = "Rodrigo")
    private String firstName;

    @NotEmpty(message = "{person.lastName.notempty}")
    @Size(min = 2, max = 100, message = "{person.lastName.size}")
    @ApiModelProperty(notes = "Sobrenome", required = true, example = "Peleias")
    private String lastName;

    @NotEmpty(message = "{person.cpf.notempty}")
    @CPF(message = "{person.cpf.format}")
    @ApiModelProperty(notes = "CPF", required = true, example = "767.083.200-29")
    private String cpf;

    @NotEmpty(message = "{person.birthDate.notempty}")
    @CheckDateFormat(pattern = "dd/MM/yyyy", message = "{person.birthDate.format}")
    @ApiModelProperty(notes = "Data de nascimento", required = true, example = "23/03/1988")
    private String birthDate;

    @Valid
    @NotNull(message = "{person.address.notempty}")
    @ApiModelProperty(notes = "Endereço", required = true)
    private AddressDTO address;

    @Valid
    @NotEmpty(message = "{person.phones.notempty}")
    @ApiModelProperty(notes = "Lista de telefones", required = true)
    private List<PhoneDTO> phones;

    @Valid
    @NotEmpty(message = "{person.emails.notempty}")
    @ApiModelProperty(notes = "Lista de e-mails", required = true)
    private List<EmailDTO> emails;

    @JsonIgnore
    public String getOnlyCPFNumber() {
        return this.cpf.replace("-", "").replace(".", "");
    }
}
