package br.com.miraeducacao.usuarios.api.repository;

import br.com.miraeducacao.usuarios.api.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PersonRepository extends JpaRepository<Person, Long> {

    Person findByCpf(String cpf);

    List<Person> findAllByActiveIsTrue();

    @Modifying
    @Transactional
    @Query("update Person p set p.active = false where p.id = :id")
    int inactivePersonById(@Param("id") Long id);

    List<Person> findByFirstNameOrLastNameOrCpfAndActiveIsTrue(String firstName, String lastName, String cpf);
}
