package br.com.miraeducacao.usuarios.api.exception;

public class UserAlreadyExistsException extends Exception{

    public UserAlreadyExistsException(String attribute) {
        super("Usuário com " + attribute + " já cadastrado no sistema");
    }
}
