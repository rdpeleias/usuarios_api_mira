package br.com.miraeducacao.usuarios.api.controllers;

import br.com.miraeducacao.usuarios.api.dto.PersonDTO;
import br.com.miraeducacao.usuarios.api.exception.UserAlreadyExistsException;
import br.com.miraeducacao.usuarios.api.exception.UserNotExistsException;
import br.com.miraeducacao.usuarios.api.response.Response;
import br.com.miraeducacao.usuarios.api.service.PersonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@Api(value = "People", description = "API REST para a criação e gerenciamento de pessoas.")
@RequestMapping(value = "/api/people", produces = "application/json")
public class PersonController {

    @Autowired
    private PersonService personService;

    @ApiOperation(value = "Cria uma nova pessoa.")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Pessoa criada com sucesso."),
            @ApiResponse(code = 400, message = "Erros de validação de campos."),
            @ApiResponse(code = 422, message = "Pessoa já existente não pode ser cadastrada."),
    })
    @PostMapping
    public ResponseEntity<Response<PersonDTO>> create(@Valid @RequestBody PersonDTO personDTO) {
        Response<PersonDTO> response = new Response<>();
        try {
            PersonDTO createdPerson = personService.create(personDTO);
            response.setData(createdPerson);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (UserAlreadyExistsException e) {
            response.getErrors().add(e.getLocalizedMessage());
            return ResponseEntity.unprocessableEntity().body(response);
        }
    }

    @ApiOperation(value = "Cria uma lista de novas pessoas (bulk).")
    @PostMapping(value = "/bulk")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Pessoa criados com sucesso."),
            @ApiResponse(code = 400, message = "Erros de validação de campos."),
            @ApiResponse(code = 422, message = "CPF informado já cadastrado em outra pessoa."),
    })
    public ResponseEntity<Response<List<PersonDTO>>> bulkCreate(@Valid @RequestBody List<PersonDTO> peopleDTO) {
        Response<List<PersonDTO>> response = new Response<>();
        try {
            List<PersonDTO> createdPeople = personService.bulkCreate(peopleDTO);
            response.setData(createdPeople);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (UserAlreadyExistsException e) {
            response.getErrors().add(e.getLocalizedMessage());
            return ResponseEntity.unprocessableEntity().body(response);
        }
    }

    @ApiOperation(value = "Retorna todas as pessoas cadastradas ativas.")
    @GetMapping
    @ApiResponse(code = 200, message = "Retorna com sucesso lista de pessoas ativas.")
    public ResponseEntity<Response<List<PersonDTO>>> findAllEnabled() {
        Response<List<PersonDTO>> response = new Response<>();
        List<PersonDTO> activePeople = personService.findAllActive();
        response.setData(activePeople);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "Retorna todas as pessoas cadastradas ativas por nome, sobrenome e/ou CPF")
    @GetMapping(value = "/search")
    @ApiResponse(code = 200, message = "Retorna com sucesso lista de pessoas ativas.")
    public ResponseEntity<Response<List<PersonDTO>>> findByFirstNameOrLastNameOrCpf(
            @RequestParam(value = "firstName", required = false) String firstName,
            @RequestParam(value = "lastName", required = false) String lastName,
            @RequestParam(value = "cpf", required = false) String cpf) {
        Response<List<PersonDTO>> response = new Response<>();
        List<PersonDTO> foundPeople = personService.findByFirstNameOrLastNameOrCpfAndActiveIsTrue(firstName, lastName, cpf);
        response.setData(foundPeople);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "Exclui logicamente uma pessoa cadastrada no sistema.")
    @DeleteMapping(value = "/{id}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna com sucesso lista de pessoas ativas."),
            @ApiResponse(code = 404, message = "ID de usuário informado não encontrado.")
    })
    public ResponseEntity<Response<String>> inactivatePerson(@PathVariable("id") Long id) {
        try {
            personService.inactivePersonById(id);
            return ResponseEntity.ok().build();
        } catch (UserNotExistsException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @ApiOperation(value = "Atualiza uma pessoa com ID informado")
    @PutMapping(value = "/{id}")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Usuário atualizado com sucesso."),
            @ApiResponse(code = 400, message = "Erros de validação de campos."),
            @ApiResponse(code = 404, message = "ID de usuário informado não encontrado."),
            @ApiResponse(code = 422, message = "CPF informado já cadastrado em outra pessoa.")
    })
    public ResponseEntity<Response<PersonDTO>> update(@PathVariable("id") Long id, @Valid @RequestBody PersonDTO personDTO) {
        Response<PersonDTO> response = new Response<>();
        try {
            PersonDTO createdPerson = personService.update(id, personDTO);
            response.setData(createdPerson);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (UserAlreadyExistsException e) {
            response.getErrors().add(e.getLocalizedMessage());
            return ResponseEntity.unprocessableEntity().body(response);
        } catch (UserNotExistsException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @ApiOperation(value = "Trata erros de validação encontrados no cadastro e atualização de novas pessoas.")
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<Response<String>> handleErrors(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        Response<String> response = new Response<>();
        result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));

        return ResponseEntity.badRequest().body(response);
    }

}
