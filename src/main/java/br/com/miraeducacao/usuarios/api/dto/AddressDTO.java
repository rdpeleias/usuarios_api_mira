package br.com.miraeducacao.usuarios.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddressDTO implements Serializable {

    @NotEmpty(message = "{address.name.notempty}")
    @Size(max = 100, message = "{address.name.size}")
    @ApiModelProperty(notes = "Logradouro", required = true, example = "Avenida Jabaquara")
    private String name;

    @NotEmpty(message = "{address.number.notempty}")
    @Size(min = 1, max = 5, message = "{address.number.size}")
    @ApiModelProperty(notes = "Número do logradouro", required = true, example = "200")
    private String number;

    @Size(max = 100, message = "{address.complement.max}")
    @ApiModelProperty(notes = "Complemento", example = "Apto. 50")
    private String complement;

    @NotEmpty(message = "{address.postalcode.notempty}")
    @Size(min = 9, max = 9, message = "{address.postalcode.size}")
    @Pattern(regexp = "(\\d{5}-\\d{3})", message = "{address.postalcode.pattern}")
    @ApiModelProperty(notes = "CEP", required = true, example = "03134-000")
    private String postalCode;

    @NotEmpty(message = "{address.city.notempty}")
    @Size(min = 2, max = 30, message = "{address.city.max}")
    @ApiModelProperty(notes = "Cidade", required = true, example = "São Paulo")
    private String city;

    @NotEmpty(message = "{address.state.notempty}")
    @Size(min = 2, max = 2, message = "{address.city.max}")
    @ApiModelProperty(notes = "Estado", required = true, example = "SP")
    private String state;

    @NotEmpty(message = "{address.country.notempty}")
    @Size(max = 50, message = "{address.coutry.max}")
    @ApiModelProperty(notes = "País", required = true, example = "Brasil")
    private String country;
}


