package br.com.miraeducacao.usuarios.api.exception;

public class UserNotExistsException extends Exception {

    public UserNotExistsException() {
        super("Usuário informado não existe no sistema.");
    }
}
