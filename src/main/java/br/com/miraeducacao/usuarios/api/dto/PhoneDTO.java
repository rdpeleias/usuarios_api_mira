package br.com.miraeducacao.usuarios.api.dto;

import br.com.miraeducacao.usuarios.api.domain.PhoneType;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PhoneDTO implements Serializable {

    @Enumerated(EnumType.STRING)
    @NotNull(message = "{phone.phonetype.notempty}")
    @ApiModelProperty(notes = "Tipo do telefone", example = "MOBILE", required = true)
    private PhoneType phoneType;

    @NotEmpty(message = "{phone.number.notempty}")
    @Pattern(regexp = "\\(\\d{2}\\)(\\d{4}|\\d{5})-(\\d{4})", message = "{phone.number.pattern}")
    @Size(min = 13, max = 14, message = "{phone.number.size}")
    @ApiModelProperty(notes = "NÚMERO do telefone", required = true, example = "(11)99999-9999")
    private String number;
}
