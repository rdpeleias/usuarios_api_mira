package br.com.miraeducacao.usuarios.api.config;

import org.modelmapper.AbstractConverter;
import org.modelmapper.AbstractProvider;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.Provider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Configuration
public class ModalMapperConfig {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();

        Provider<Date> dateProvider = new AbstractProvider<Date>() {
            @Override
            protected Date get() {
                return new Date();
            }
        };

        Converter<String, Date> toStringDate = new AbstractConverter<String, Date>() {
            @Override
            protected Date convert(String value) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    return simpleDateFormat.parse(value);
                } catch (ParseException e) {
                    throw new IllegalArgumentException("Formato de cpf inválido.");
                }
            }
        };

        Converter<Date, String> toDateString = new AbstractConverter<Date, String>() {
            @Override
            protected String convert(Date date) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                return simpleDateFormat.format(date);
            }
        };
        modelMapper.createTypeMap(String.class, Date.class);
        modelMapper.addConverter(toStringDate);
        modelMapper.addConverter(toDateString);
        modelMapper.getTypeMap(String.class, Date.class).setProvider(dateProvider);
        return modelMapper;
    }
}
