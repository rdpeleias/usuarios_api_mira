package br.com.miraeducacao.usuarios.api.domain;

import lombok.Getter;

@Getter
public enum PhoneType {

    MOBILE("Celular"),
    HOME("Residencial"),
    COMMERCIAL("Comercial");

    private String description;

    private PhoneType(String description) {
        this.description = description;
    }
}
