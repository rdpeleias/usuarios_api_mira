package br.com.miraeducacao.usuarios.api.service;

import br.com.miraeducacao.usuarios.api.dto.PersonDTO;
import br.com.miraeducacao.usuarios.api.exception.UserAlreadyExistsException;
import br.com.miraeducacao.usuarios.api.exception.UserNotExistsException;

import java.util.List;

public interface PersonService {

    PersonDTO create(PersonDTO personDTO) throws UserAlreadyExistsException;

    List<PersonDTO> bulkCreate(List<PersonDTO> people) throws UserAlreadyExistsException;

    List<PersonDTO> findAllActive();

    void inactivePersonById(Long id) throws UserNotExistsException;

    PersonDTO update(Long id, PersonDTO personDTO) throws UserNotExistsException, UserAlreadyExistsException;

    List<PersonDTO> findByFirstNameOrLastNameOrCpfAndActiveIsTrue(String firstName, String lastName, String cpf);

}
