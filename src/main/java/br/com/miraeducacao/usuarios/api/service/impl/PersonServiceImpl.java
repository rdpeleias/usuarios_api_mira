package br.com.miraeducacao.usuarios.api.service.impl;

import br.com.miraeducacao.usuarios.api.domain.Person;
import br.com.miraeducacao.usuarios.api.dto.EmailDTO;
import br.com.miraeducacao.usuarios.api.dto.PersonDTO;
import br.com.miraeducacao.usuarios.api.exception.UserAlreadyExistsException;
import br.com.miraeducacao.usuarios.api.exception.UserNotExistsException;
import br.com.miraeducacao.usuarios.api.repository.PersonRepository;
import br.com.miraeducacao.usuarios.api.service.PersonService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    @CacheEvict(cacheNames = "people", allEntries = true)
    public PersonDTO create(PersonDTO personDTO) throws UserAlreadyExistsException {
        if (personRepository.findByCpf(personDTO.getOnlyCPFNumber()) != null) {
            throw new UserAlreadyExistsException("CPF " + personDTO.getCpf());
        }
        return createOrUpdate(personDTO);
    }

    @Override
    public List<PersonDTO> bulkCreate(List<PersonDTO> peopleDTO) throws UserAlreadyExistsException {
        List<Person> people = peopleDTO.stream()
                .map(personDTO -> convertToEntity(personDTO))
                .collect(Collectors.toList());
        try {
            people = personRepository.saveAll(people);
        } catch (DataIntegrityViolationException e) {
            throw new UserAlreadyExistsException("CPF");
        }

        return people.stream()
                .map(person -> convertToDTO(person))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable(cacheNames = "people")
    public List<PersonDTO> findAllActive() {
        return personRepository.findAllByActiveIsTrue().stream()
                .map(person -> this.convertToDTO(person))
                .collect(Collectors.toList());
    }

    @Override
    @CacheEvict(value = "people", allEntries = true)
    public void inactivePersonById(Long id) throws UserNotExistsException {
        if (!this.personRepository.findById(id).isPresent()) {
            throw new UserNotExistsException();
        }
        this.personRepository.inactivePersonById(id);
    }

    @Override
    @CachePut("people")
    public PersonDTO update(Long id, PersonDTO personDTO) throws UserNotExistsException, UserAlreadyExistsException {
        if (!this.personRepository.findById(id).isPresent()) {
            throw new UserNotExistsException();
        }
        Person personByCpf = personRepository.findByCpf(personDTO.getOnlyCPFNumber());
        if (!personByCpf.isActive()) {
            throw new UserNotExistsException();
        }
        if (personByCpf != null && !personByCpf.getId().equals(id)) {
            throw new UserAlreadyExistsException("CPF " + personDTO.getCpf());
        }
        personDTO.setId(id);
        return this.createOrUpdate(personDTO);
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable(cacheNames = "people")
    public List<PersonDTO> findByFirstNameOrLastNameOrCpfAndActiveIsTrue(String firstName, String lastName, String cpf) {
        return personRepository.findByFirstNameOrLastNameOrCpfAndActiveIsTrue(firstName, lastName, cpf)
                .stream()
                .map(person -> this.convertToDTO(person))
                .collect(Collectors.toList());
    }

    private PersonDTO createOrUpdate(PersonDTO personDTO) {
        personDTO.setCpf(personDTO.getOnlyCPFNumber());
        Person person = convertToEntity(personDTO);
        person.setActive(true);
        Person createdPerson = personRepository.save(person);
        return convertToDTO(createdPerson);
    }

    private Person convertToEntity(PersonDTO personDTO) {
        Person person = modelMapper.map(personDTO, Person.class);
        person.setEmails(new ArrayList<>());
        List<EmailDTO> emails = personDTO.getEmails();
        emails.stream().forEach(email -> person.getEmails().add(email.getEmail()));

        return person;
    }

    private PersonDTO convertToDTO(Person person) {
        PersonDTO personDTO = modelMapper.map(person, PersonDTO.class);
        List<EmailDTO> emailDTOS = new ArrayList<>();
        person.getEmails().forEach(email -> {
            EmailDTO emailDTO = EmailDTO.builder().email(email).build();
            emailDTOS.add(emailDTO);
        });
        personDTO.setEmails(emailDTOS);
        return personDTO;
    }

}
