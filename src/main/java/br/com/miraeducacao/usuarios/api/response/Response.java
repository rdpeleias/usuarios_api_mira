package br.com.miraeducacao.usuarios.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Response<T> {

    @Getter
    private T data;

    private List<String> errors;

    public List<String> getErrors() {
        if (this.errors == null) {
            this.errors = new ArrayList<>();
        }
        return this.errors;
    }
}
