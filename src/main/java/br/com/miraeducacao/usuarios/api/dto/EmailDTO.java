package br.com.miraeducacao.usuarios.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmailDTO implements Serializable {

    @NotEmpty(message = "E-mail é um campo obrigatório.")
    @Email(message = "Formato do e-mail informado está inválido.")
    @ApiModelProperty(notes = "E-mail", required = true, example = "teste@teste.com")
    private String email;
}
