package br.com.miraeducacao.usuarios.api.utils;

import br.com.miraeducacao.usuarios.api.domain.PhoneType;
import br.com.miraeducacao.usuarios.api.dto.AddressDTO;
import br.com.miraeducacao.usuarios.api.dto.EmailDTO;
import br.com.miraeducacao.usuarios.api.dto.PersonDTO;
import br.com.miraeducacao.usuarios.api.dto.PhoneDTO;

import java.util.Arrays;
import java.util.List;

public class PersonDTOUtils {

    public static PersonDTO buildPerson() {
        return PersonDTO.builder()
                .firstName("Rodrigo")
                .lastName("Peleias")
                .birthDate("23/03/1988")
                .cpf("369.333.878-79")
                .address(buildAddress())
                .emails(buildEmails())
                .phones(buildPhones())
                .build();
    }

    public static List<PhoneDTO> buildPhones() {
        PhoneDTO home = PhoneDTO.builder()
                .number("(11)1234-5678")
                .phoneType(PhoneType.HOME).build();
        PhoneDTO mobile = PhoneDTO.builder()
                .number("(11)99999-5678")
                .phoneType(PhoneType.MOBILE).build();
        return Arrays.asList(home, mobile);
    }

    public static AddressDTO buildAddress() {
        return AddressDTO.builder()
                .name("Jabaquara Avenue")
                .number("200")
                .complement("apt 20")
                .postalCode("12345-678")
                .country("Brazil")
                .city("São Paulo")
                .state("SP").build();
    }

    public static List<EmailDTO> buildEmails() {
        EmailDTO firstEmail = EmailDTO.builder().email("rodrigo@teste.com").build();
        EmailDTO secondEmail = EmailDTO.builder().email("teste@segundo.com").build();
        return Arrays.asList(firstEmail, secondEmail);
    }
}
