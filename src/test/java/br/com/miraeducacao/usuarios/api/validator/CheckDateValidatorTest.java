package br.com.miraeducacao.usuarios.api.validator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CheckDateValidatorTest {

    private CheckDateValidator checkDateValidator;

    @Before
    public void setUp() {
        CheckDateFormat checkDateFormat = mock(CheckDateFormat.class);
        when(checkDateFormat.pattern()).thenReturn("dd/MM/yyyy");

        this.checkDateValidator = new CheckDateValidator();
        this.checkDateValidator.initialize(checkDateFormat);
    }

    @Test
    public void testCheckValidDate() {
        assertTrue(this.checkDateValidator.isValid("23/03/1988", null));
    }

    @Test
    public void testCheckInvalidDate() {
        assertFalse(this.checkDateValidator.isValid("23/1988/03", null));
    }
}
