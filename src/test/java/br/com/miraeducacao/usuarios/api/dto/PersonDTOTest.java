package br.com.miraeducacao.usuarios.api.dto;

import org.junit.Before;
import org.junit.Test;

import static br.com.miraeducacao.usuarios.api.utils.PersonDTOUtils.buildPerson;
import static org.junit.Assert.assertEquals;

public class PersonDTOTest {

    private PersonDTO personDTO;

    @Before
    public void setUp() {
        this.personDTO = buildPerson();
    }

    @Test
    public void testGetOnlyCPFNumberWIthCorrectMask() {
        assertEquals("36933387879", this.personDTO.getOnlyCPFNumber());
    }

    @Test
    public void testGetOnlyCPFNumberWIthMixedMask() {
        this.personDTO.setCpf("3-69.33387.87-9");
        assertEquals("36933387879", this.personDTO.getOnlyCPFNumber());
    }
}
