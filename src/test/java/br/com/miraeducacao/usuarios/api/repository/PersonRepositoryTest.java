package br.com.miraeducacao.usuarios.api.repository;

import br.com.miraeducacao.usuarios.api.domain.Person;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static br.com.miraeducacao.usuarios.api.utils.PersonUtils.buildPerson;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class PersonRepositoryTest {

    @Autowired
    private PersonRepository personRepository;

    private Person person;

    @Before
    public void setUp() {
        this.person = buildPerson();
    }

    @After
    public void tearDown() {
        this.personRepository.deleteAll();
    }

    @Test
    public void testSavePerson() {
        Person savedPerson = this.personRepository.save(person);

        assertNotNull(savedPerson.getId());
        assertEquals(person.getFirstName(), savedPerson.getFirstName());
        assertEquals(person.getLastName(), savedPerson.getLastName());
        assertEquals(person.getCpf(), savedPerson.getCpf());
        assertEquals(person.getAddress(), savedPerson.getAddress());
        assertEquals(person.getPhones(), savedPerson.getPhones());
    }

    @Test
    public void testGetAllActivePeople() {
        person.setActive(true);
        person = this.personRepository.save(person);

        List<Person> activePeople = this.personRepository.findAllByActiveIsTrue();

        assertFalse(activePeople.isEmpty());
        assertEquals(1, activePeople.size());
        assertEquals(person.getId(), activePeople.get(0).getId());
    }

    @Test
    public void testInactivePerson() {
        Person savedPerson = this.personRepository.save(person);
        this.personRepository.inactivePersonById(savedPerson.getId());

        Optional<Person> inactivePerson = this.personRepository.findById(savedPerson.getId());

        assertFalse(inactivePerson.get().isActive());
    }

    @Test
    public void testFindPersonByFirstName() {
        this.personRepository.save(person);
        List<Person> foundPeople = this.personRepository.findByFirstNameOrLastNameOrCpfAndActiveIsTrue("Rodrigo", null, null);

        assertFalse(foundPeople.isEmpty());
        assertEquals(1, foundPeople.size());
        assertEquals(person.getFirstName(), foundPeople.get(0).getFirstName());
    }

    @Test
    public void testFindPersonByFirstNameAndLastNameAndCpf() {
        this.personRepository.save(person);
        List<Person> foundPeople = this.personRepository.findByFirstNameOrLastNameOrCpfAndActiveIsTrue("Rodrigo", "Peleias", "369.333.878-79");

        assertFalse(foundPeople.isEmpty());
        assertEquals(1, foundPeople.size());
        assertEquals(person.getFirstName(), foundPeople.get(0).getFirstName());
    }

    @Test
    public void testFindPeopleByFirstNameOrCpf() {
        this.personRepository.save(person);

        Person anotherPerson = buildPerson();
        anotherPerson.setCpf("368.345.345-80");
        anotherPerson.setFirstName("Ronaldo");
        anotherPerson.setLastName("Nazário");

        List<Person> foundPeople = this.personRepository.findByFirstNameOrLastNameOrCpfAndActiveIsTrue("Rodrigo", null, "368.345.345-80");

        assertFalse(foundPeople.isEmpty());
        assertEquals(1, foundPeople.size());
    }

    @Test
    public void testNotFindAnyPeople() {
        this.personRepository.save(person);

        Person anotherPerson = buildPerson();
        anotherPerson.setCpf("368.345.345-80");
        anotherPerson.setFirstName("Ronaldo");
        anotherPerson.setLastName("Nazário");

        List<Person> foundPeople = this.personRepository.findByFirstNameOrLastNameOrCpfAndActiveIsTrue("Paulo", "Roberto", "335.345.345-80");

        assertTrue(foundPeople.isEmpty());
    }
}
