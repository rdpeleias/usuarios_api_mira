package br.com.miraeducacao.usuarios.api.services;

import br.com.miraeducacao.usuarios.api.domain.Person;
import br.com.miraeducacao.usuarios.api.dto.PersonDTO;
import br.com.miraeducacao.usuarios.api.exception.UserAlreadyExistsException;
import br.com.miraeducacao.usuarios.api.exception.UserNotExistsException;
import br.com.miraeducacao.usuarios.api.repository.PersonRepository;
import br.com.miraeducacao.usuarios.api.service.PersonService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static br.com.miraeducacao.usuarios.api.utils.PersonDTOUtils.buildPerson;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class PersonServiceTest {

    @Autowired
    private PersonService personService;

    @Autowired
    private PersonRepository personRepository;

    private PersonDTO personDTO;

    @Before
    public void setUp() {
        personDTO = buildPerson();
    }

    @After
    public void tearDown() {
        this.personRepository.deleteAll();
    }

    @Test
    public void testCreatePerson() throws UserAlreadyExistsException {
        PersonDTO createdPersonDTO = this.personService.create(this.personDTO);

        assertNotNull(createdPersonDTO.getId());
        assertEquals(personDTO.getFirstName(), createdPersonDTO.getFirstName());
        assertEquals(personDTO.getLastName(), createdPersonDTO.getLastName());
        assertEquals(personDTO.getCpf(), createdPersonDTO.getCpf());
        assertEquals(personDTO.getEmails(), createdPersonDTO.getEmails());
        assertEquals(personDTO.getBirthDate(), createdPersonDTO.getBirthDate());
        assertEquals(personDTO.getAddress(), createdPersonDTO.getAddress());
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void testBulkCreatePeopleWithRepeatedCpf() throws UserAlreadyExistsException {
        PersonDTO createdPersonDTO = buildPerson();
        PersonDTO anotherCreatedPersonDTO = buildPerson();
        this.personService.bulkCreate(Arrays.asList(createdPersonDTO, anotherCreatedPersonDTO));
    }

    @Test
    public void testBulkCreatePeople() throws UserAlreadyExistsException {
        PersonDTO createdPersonDTO = buildPerson();
        PersonDTO anotherCreatedPersonDTO = buildPerson();
        anotherCreatedPersonDTO.setCpf("376.388.678-79");
        List<PersonDTO> createdPeople = this.personService.bulkCreate(Arrays.asList(createdPersonDTO, anotherCreatedPersonDTO));

        assertFalse(createdPeople.isEmpty());
        assertEquals(2, createdPeople.size());
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void testCreatePersonWithDoubleCpf() throws UserAlreadyExistsException {
        this.personService.create(this.personDTO);
        this.personService.create(this.personDTO);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testCreatePersonWithoutRequiredField() throws UserAlreadyExistsException {
        this.personDTO.setLastName(null);
        this.personService.create(this.personDTO);
    }

    @Test
    public void testGetAllActivePeople() throws UserAlreadyExistsException{
        PersonDTO createdPersonDTO = this.personService.create(this.personDTO);
        List<PersonDTO> activePeople = this.personService.findAllActive();

        assertFalse(activePeople.isEmpty());
        assertEquals(createdPersonDTO.getId(), activePeople.get(0).getId());
    }

    @Test
    public void testGetAllActivePeopleByFirstName() throws UserAlreadyExistsException{
        PersonDTO createdPersonDTO = this.personService.create(this.personDTO);
        List<PersonDTO> activePeople = this.personService.findByFirstNameOrLastNameOrCpfAndActiveIsTrue("Rodrigo", null, null);

        assertFalse(activePeople.isEmpty());
        assertEquals(createdPersonDTO.getId(), activePeople.get(0).getId());
    }

    @Test
    public void testGetAllActivePeopleByFirstNameAndLastNameAndCpf() throws UserAlreadyExistsException{
        PersonDTO createdPersonDTO = this.personService.create(this.personDTO);
        List<PersonDTO> activePeople = this.personService.findByFirstNameOrLastNameOrCpfAndActiveIsTrue("Rodrigo", "Peleias", null);

        assertFalse(activePeople.isEmpty());
        assertEquals(createdPersonDTO.getId(), activePeople.get(0).getId());
    }

    @Test
    public void testDeletePersonLogically() throws UserNotExistsException, UserAlreadyExistsException {
        PersonDTO createdPersonDTO = this.personService.create(this.personDTO);

        this.personService.inactivePersonById(createdPersonDTO.getId());

        Optional<Person> inactivatedPerson = this.personRepository.findById(createdPersonDTO.getId());

        assertFalse(inactivatedPerson.get().isActive());
    }

    @Test(expected = UserNotExistsException.class)
    public void testDeleteWhenNotFoundUserInformed() throws UserNotExistsException {
        this.personService.inactivePersonById(20L);
    }

    @Test
    public void testUpdatePerson() throws UserAlreadyExistsException, UserNotExistsException {
        PersonDTO createdPersonDTO = this.personService.create(this.personDTO);

        createdPersonDTO.setFirstName("Ronaldo");
        PersonDTO updatedPerson = this.personService.update(createdPersonDTO.getId(), createdPersonDTO);

        assertNotNull(updatedPerson.getId());
        assertNotEquals(personDTO.getFirstName(), updatedPerson.getFirstName());
        assertEquals(updatedPerson.getFirstName(), "Ronaldo");
        assertEquals(personDTO.getLastName(), updatedPerson.getLastName());
        assertEquals(personDTO.getCpf(), updatedPerson.getCpf());
        assertEquals(personDTO.getEmails(), updatedPerson.getEmails());
        assertEquals(personDTO.getBirthDate(), updatedPerson.getBirthDate());
        assertEquals(personDTO.getAddress(), updatedPerson.getAddress());
    }

    @Test(expected = UserNotExistsException.class)
    public void testUpdatePersonInvalidID() throws UserAlreadyExistsException, UserNotExistsException {
        PersonDTO createdPersonDTO = this.personService.create(this.personDTO);

        createdPersonDTO.setFirstName("Ronaldo");
        this.personService.update(20L, createdPersonDTO);
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void testUpdatePersonWithExistingCPF() throws UserAlreadyExistsException, UserNotExistsException {
        this.personService.create(this.personDTO);

        PersonDTO anotherPersonDTO = buildPerson();
        anotherPersonDTO.setFirstName("Ronaldo");
        anotherPersonDTO.setLastName("Nazário");
        anotherPersonDTO.setCpf("333.678.796-15");

        anotherPersonDTO = this.personService.create(anotherPersonDTO);
        anotherPersonDTO.setCpf("369.333.878-79");

        this.personService.update(anotherPersonDTO.getId(), anotherPersonDTO);
    }
}
