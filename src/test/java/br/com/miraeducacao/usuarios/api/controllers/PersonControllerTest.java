package br.com.miraeducacao.usuarios.api.controllers;

import br.com.miraeducacao.usuarios.api.dto.PersonDTO;
import br.com.miraeducacao.usuarios.api.exception.UserAlreadyExistsException;
import br.com.miraeducacao.usuarios.api.exception.UserNotExistsException;
import br.com.miraeducacao.usuarios.api.service.PersonService;
import br.com.miraeducacao.usuarios.api.utils.PersonDTOUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class PersonControllerTest {

    private static final String URL_BASE = "/api/people/";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PersonService personService;

    private ObjectMapper objectMapper;

    private PersonDTO personDTO;

    @Before
    public void setUp() {
        this.objectMapper = new ObjectMapper();
        personDTO = PersonDTOUtils.buildPerson();
        personDTO.setId(1L);
    }

    @Test
    public void testCreatePerson() throws Exception {
        given(this.personService.create(any(PersonDTO.class))).willReturn(personDTO);
        String personDTOJson = this.objectMapper.writeValueAsString(personDTO);

        mvc.perform(post(URL_BASE)
                .content(personDTOJson)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.data.id").value(personDTO.getId()))
                .andExpect(jsonPath("$.data.firstName").value(personDTO.getFirstName()))
                .andExpect(jsonPath("$.data.lastName").value(personDTO.getLastName()))
                .andExpect(jsonPath("$.data.cpf").value(personDTO.getCpf()))
                .andExpect(jsonPath("$.data.address").value(personDTO.getAddress()));
    }

    @Test
    public void testCreatePersonWithoutRequiredField() throws Exception {
        this.personDTO.setLastName(null);
        given(this.personService.create(any(PersonDTO.class))).willReturn(personDTO);
        String personDTOJson = this.objectMapper.writeValueAsString(personDTO);

        mvc.perform(post(URL_BASE)
                .content(personDTOJson)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors").value("Sobrenome é um campo obrigatório."));
    }

    @Test
    public void testCreatePersonWithUserAlreadyRegistered() throws Exception {
        given(this.personService.create(any(PersonDTO.class))).willThrow(new UserAlreadyExistsException("CPF " + this.personDTO.getCpf()));
        String personDTOJson = this.objectMapper.writeValueAsString(personDTO);

        mvc.perform(post(URL_BASE)
                .content(personDTOJson)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.errors").value("Usuário com CPF 369.333.878-79 já cadastrado no sistema"));
    }

    @Test
    public void testCreatePersonWithInvalidCPF() throws Exception {
        this.personDTO.setCpf("12345678891011");
        given(this.personService.create(any(PersonDTO.class))).willReturn(personDTO);
        String personDTOJson = this.objectMapper.writeValueAsString(personDTO);

        mvc.perform(post(URL_BASE)
                .content(personDTOJson)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors").value("Formato do CPF informado é inválido."));
    }

    @Test
    public void testCreatePersonWithInvalidBirthDate() throws Exception {
        this.personDTO.setBirthDate("1988/03/23");
        given(this.personService.create(any(PersonDTO.class))).willReturn(personDTO);
        String personDTOJson = this.objectMapper.writeValueAsString(personDTO);

        mvc.perform(post(URL_BASE)
                .content(personDTOJson)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors").value("Formato da data informada é inválido."));
    }

    @Test
    public void testBulkCreatePeople() throws Exception {
        PersonDTO anotherPersonDTO = PersonDTOUtils.buildPerson();
        List<PersonDTO> peopleDTO = Arrays.asList(personDTO, anotherPersonDTO);
        given(this.personService.bulkCreate(any(List.class))).willReturn(peopleDTO);
        String peopleJSONDTO = this.objectMapper.writeValueAsString(peopleDTO);

        mvc.perform(post(URL_BASE + "bulk")
                .content(peopleJSONDTO)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testBulkCreatePeopleWithError() throws Exception {
        PersonDTO anotherPersonDTO = PersonDTOUtils.buildPerson();
        List<PersonDTO> peopleDTO = Arrays.asList(personDTO, anotherPersonDTO);
        given(this.personService.bulkCreate(any(List.class))).willThrow(new UserAlreadyExistsException("CPF"));
        String peopleJSONDTO = this.objectMapper.writeValueAsString(peopleDTO);

        mvc.perform(post(URL_BASE + "bulk")
                .content(peopleJSONDTO)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.errors").value("Usuário com CPF já cadastrado no sistema"));
    }


    @Test
    public void testGetAllActivePeople() throws Exception {
        given(this.personService.findAllActive()).willReturn(new ArrayList<>());

        mvc.perform(get(URL_BASE)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetAllActivePeopleByFirstNameOrLastNameOrCpf() throws Exception {
        given(this.personService.findByFirstNameOrLastNameOrCpfAndActiveIsTrue("Rodrigo", "Peleias", "368.345.356-80")).willReturn(new ArrayList<>());

        mvc.perform(get(URL_BASE + "/search")
                .param("firstName", "Rodrigo")
                .param("lastName", "Peleias")
                .param("cpf", "368.345.356-80")
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testInactivatePerson() throws Exception {
        mvc.perform(delete(URL_BASE + 1L)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(this.personService, times(1)).inactivePersonById(1L);
    }

    @Test
    public void testInactivatePersonInvalidId() throws Exception {
        doThrow(new UserNotExistsException()).when(this.personService).inactivePersonById(20L);

        mvc.perform(delete(URL_BASE + 20L)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(this.personService, times(1)).inactivePersonById(20L);
    }

    @Test
    public void testUpdatePerson() throws Exception {
        given(this.personService.update(anyLong(), any(PersonDTO.class))).willReturn(personDTO);
        String personDTOJson = this.objectMapper.writeValueAsString(personDTO);

        mvc.perform(put(URL_BASE + 1L)
                .content(personDTOJson)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.data.id").value(personDTO.getId()))
                .andExpect(jsonPath("$.data.firstName").value(personDTO.getFirstName()))
                .andExpect(jsonPath("$.data.lastName").value(personDTO.getLastName()))
                .andExpect(jsonPath("$.data.cpf").value(personDTO.getCpf()))
                .andExpect(jsonPath("$.data.address").value(personDTO.getAddress()));
    }

    @Test
    public void testUpdatePersonInvalidId() throws Exception {
        given(this.personService.update(anyLong(), any(PersonDTO.class))).willThrow(new UserNotExistsException());
        String personDTOJson = this.objectMapper.writeValueAsString(personDTO);

        mvc.perform(put(URL_BASE + 10L)
                .content(personDTOJson)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdatePersonAlreadyExists() throws Exception {
        given(this.personService.update(anyLong(), any(PersonDTO.class))).willThrow(new UserAlreadyExistsException("CPF 369.333.878-79"));
        String personDTOJson = this.objectMapper.writeValueAsString(personDTO);

        mvc.perform(put(URL_BASE + 10L)
                .content(personDTOJson)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.errors").value("Usuário com CPF 369.333.878-79 já cadastrado no sistema"));
    }
}
