package br.com.miraeducacao.usuarios.api.utils;

import br.com.miraeducacao.usuarios.api.domain.Address;
import br.com.miraeducacao.usuarios.api.domain.Person;
import br.com.miraeducacao.usuarios.api.domain.Phone;
import br.com.miraeducacao.usuarios.api.domain.PhoneType;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class PersonUtils {

    public static Person buildPerson() {
        return Person.builder()
                .firstName("Rodrigo")
                .lastName("Peleias")
                .birthDate(new Date())
                .cpf("369.333.878-79")
                .address(buildAddress())
                .phones(buildPhones())
                .emails(buildEmails())
                .active(true)
                .build();
    }

    public static Address buildAddress() {
        return Address.builder()
                .name("Jabaquara Avenue")
                .number("200")
                .complement("Apartment 20")
                .postalCode("02123-070")
                .city("São Paulo")
                .state("SP")
                .country("Brazil").build();
    }

    public static List<Phone> buildPhones() {
        Phone mobile = Phone.builder()
                .number("99999-99999")
                .phoneType(PhoneType.MOBILE)
                .build();
        Phone home = Phone.builder()
                .number("88888-8888")
                .phoneType(PhoneType.HOME)
                .build();
        return Arrays.asList(home, mobile);
    }

    public static List<String> buildEmails() {
        return Arrays.asList("test@rodrigo.com", "example@api.com");
    }
}
