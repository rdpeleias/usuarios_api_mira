[![CircleCI](https://circleci.com/bb/rdpeleias/usuarios_api_mira/tree/master.svg?style=svg)](https://circleci.com/bb/rdpeleias/usuarios_api_mira/tree/master)


#Teste técnico - Backend 

## Tema
API para manutenção de dados de usuários.

## Formas de execução da API:

A API pode ser executada com 3 tipos de ambientes: **default**, **container** e **test**

* Execução local com Maven, com profile **default**:

    
    `mvn spring-boot:run`

    Neste profile, o projeto será executado com o banco de dados H2, no endereço `http://localhost:8080`.    
    
* Execução no Heroku:

    O projeto já se encontra implantado no Heroku. O deploy foi feito através de configuração feita e executada pelo **CircleCI**
    A configuração pode ser vista em `.circleci/config.yml`
    
    Link para execução no Heroku: [https://usersprojectmira.herokuapp.com](https://usersprojectmira.herokuapp.com)
    
* Execução através de containers:

    O projeto pode ser executado através do docker, com o profile **container**. 
    Para a execução com containers do Docker, foi utilizado o docker-compose para a execução de um container com o MySQL e outro container construído através de um **Dockerfile** para o build do projeto.
    A execuçao do container é com o comando abaixo:
       
    `docker-compose up`


## Descrição das implementações propostas para a API

* Cadastro único de pessoa via portal

 
    `POST /api/people`
    
    
    Detalhes e exemplos para execução: [https://usersprojectmira.herokuapp.com/swagger-ui.html#!/person-controller/createUsingPOST](https://usersprojectmira.herokuapp.com/swagger-ui.html#!/person-controller/createUsingPOST)
      
* Cadastro de uma lista de pessoas sendo inseridas de uma única vez

    `POST /api/people/bulk`
    
    Detalhes e exemplos para execução: [https://usersprojectmira.herokuapp.com/swagger-ui.html#!/person-controller/bulkCreateUsingPOST](https://usersprojectmira.herokuapp.com/swagger-ui.html#!/person-controller/bulkCreateUsingPOST)
    
* Retornar a lista de pessoas inseridas
    
    `GET /api/people`
    
    Detalhes e exemplos para execução: [https://usersprojectmira.herokuapp.com/swagger-ui.html#!/person-controller/findAllEnabledUsingGET](https://usersprojectmira.herokuapp.com/swagger-ui.html#!/person-controller/findAllEnabledUsingGET)
    
* Atualização de uma pessoa:

    `PUT /api/people/{id}`
    
    Detalhes e exemplos para execução: [https://usersprojectmira.herokuapp.com/swagger-ui.html#!/person-controller/updateUsingPUT](https://usersprojectmira.herokuapp.com/swagger-ui.html#!/person-controller/updateUsingPUT)
    
* Remoção de uma pessoa logicamente da base

    `DELETE /api/people/{id}`
    
    Detalhes e exemplos para execução: [https://usersprojectmira.herokuapp.com/swagger-ui.html#!/person-controller/inactivatePersonUsingDELETE](https://usersprojectmira.herokuapp.com/swagger-ui.html#!/person-controller/inactivatePersonUsingDELETE)
    
* Endpoint de busca por nome, sobrenome e/ou cpf

    `GET /api/people/search?firstName=<valor>&lastName=<valor>&cpf=<valor>`
    
    Detalhes e exemplos para execução: [https://usersprojectmira.herokuapp.com/swagger-ui.html#!/person-controller/findByFirstNameOrLastNameOrCpfUsingGET](https://usersprojectmira.herokuapp.com/swagger-ui.html#!/person-controller/findByFirstNameOrLastNameOrCpfUsingGET)
    
## Tecnologias utilizadas:

Desenvolvimento:

* Java 8
* Spring Boot 2.0.5
* Spring Data
* Bean Validation
* Banco de Dados H2 para ambientes **default** (local e Heroku) e **test**
* Banco de Dados MySQL para execução do ambiente em containers

Implantação:

* Docker
* Heroku